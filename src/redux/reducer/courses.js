/* eslint-disable no-param-reassign */
import { createSlice } from "@reduxjs/toolkit";

const coursesSlice = createSlice({
  name: "courses",
  initialState: {
    result: [],
    isLoading: false,
  },
  reducers: {
    fetchCoursesInit: (state) => {
      state.isLoading = true;
    },
    fetchCoursesSuccess: (state, action) => {
      state.result = action.payload;
      state.isLoading = false;
    },
    fetchCoursesFailure: (state) => {
      state.isLoading = false;
    },
  },
});

export const { fetchCoursesInit, fetchCoursesFailure, fetchCoursesSuccess } =
  coursesSlice.actions;

export default coursesSlice.reducer;
