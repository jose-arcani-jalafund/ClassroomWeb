import { createContext, useContext } from "react";

const StudentContext = createContext({});

function useStudentContext() {
  return useContext(StudentContext);
}

export { StudentContext, useStudentContext };
