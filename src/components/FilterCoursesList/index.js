import {
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
  RadioGroup,
  TextField,
} from "@mui/material";
import PropTypes from "prop-types";
import React, { useMemo } from "react";
import useFilterList from "../../hooks/useFilterList";
import CardList from "../CardList";
import CourseCard from "../CourseCard";

function FilterCoursesList({ inputs, originalList, listOptions, toggleForm }) {
  const { sections, radios } = inputs;
  const initialValues = {};
  for (let i = 0; i < sections.length; i += 1) {
    sections[i].inputs.map(({ label, value }) => {
      initialValues[label] = value;
      return null;
    });
  }
  for (let i = 0; i < radios.length; i += 1) {
    initialValues[radios[i].title] = 0;
  }

  const { handleChange, handleRadioChange, values, filteredList } =
    useFilterList(initialValues, originalList);

  const courseCards = useMemo(() => {
    const cards = filteredList.map((course) => {
      return (
        <Grid item key={course.id} xs={12} sm={6} md={4}>
          <CourseCard course={course} listOptions={listOptions} />
        </Grid>
      );
    });

    return cards;
    // eslint-disable-next-line
  }, [filteredList, toggleForm]);

  const renderForm = () => {
    return (
      <>
        {sections.map((s) => {
          return (
            <div key={s.id}>
              <Grid container spacing={1}>
                {s.inputs.map((i) => {
                  return (
                    <Grid item key={i.id} xs={i.xs} sm={i.sm} md={i.md}>
                      <TextField
                        type={i.type}
                        placeholder={i.placeholder}
                        onChange={handleChange}
                        value={values[i.label]}
                        name={i.label}
                        fullWidth
                      />
                    </Grid>
                  );
                })}
              </Grid>
            </div>
          );
        })}
        {radios.map((r) => {
          return (
            <div key={r.id}>
              <RadioGroup row defaultValue="none">
                {r.values.map((v) => {
                  return (
                    <FormControlLabel
                      key={v.id}
                      value={v.value}
                      control={<Checkbox />}
                      label={v.label}
                      onChange={handleRadioChange}
                      name={r.title}
                    />
                  );
                })}
              </RadioGroup>
            </div>
          );
        })}
      </>
    );
  };

  return (
    <>
      <FormGroup>{renderForm()}</FormGroup>
      <CardList layout="grid" cards={courseCards} />
    </>
  );
}

FilterCoursesList.propTypes = {
  inputs: PropTypes.shape({
    sections: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        inputs: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            placeholder: PropTypes.string,
            label: PropTypes.string,
            type: PropTypes.string,
            xs: PropTypes.number,
            sm: PropTypes.number,
            md: PropTypes.number,
          }),
        ),
      }),
    ),
    radios: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        values: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            value: PropTypes.number,
            label: PropTypes.string,
          }),
        ),
      }),
    ),
  }).isRequired,
  originalList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      coureName: PropTypes.number,
    }),
  ).isRequired,
  listOptions: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      link: PropTypes.func,
      icon: PropTypes.node,
      disabled: PropTypes.bool,
    }),
  ).isRequired,
  toggleForm: PropTypes.bool.isRequired,
};

export default FilterCoursesList;
