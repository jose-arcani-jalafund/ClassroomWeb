import React from "react";
import PropTypes from "prop-types";
import { Card, CardHeader, Avatar, Typography, Grid, Box } from "@mui/material";
import DateRangeIcon from "@mui/icons-material/DateRange";
import PersonIcon from "@mui/icons-material/Person";
import WatchLaterIcon from "@mui/icons-material/WatchLater";
import EventIcon from "@mui/icons-material/Event";

function SubjectCard({ subject }) {
  const avatarSize = 110;
  const { name, image, ...rest } = subject;
  const icons = [
    <DateRangeIcon
      sx={{
        marginLeft: 1,
      }}
    />,
    <PersonIcon
      sx={{
        marginLeft: 1,
      }}
    />,
    <WatchLaterIcon
      sx={{
        marginLeft: 1,
      }}
    />,
    <EventIcon
      sx={{
        marginLeft: 1,
      }}
    />,
  ];

  return (
    <Card sx={{ maxWidth: 500, height: 180 }}>
      <CardHeader
        avatar={
          <Avatar src={image} sx={{ width: avatarSize, height: avatarSize }} />
        }
        title={<Typography variant="h5">{name}</Typography>}
        subheader={
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            justifyContent="left"
          >
            {Object.values(rest).map((value, index) => {
              return (
                <Grid key={value} item xs={6}>
                  <Box display="flex" justifyContent="flex-end">
                    <Typography
                      sx={{
                        display: "flex",
                        justifyContent: "center",
                      }}
                    >
                      {value}
                      {icons[index]}
                    </Typography>
                  </Box>
                </Grid>
              );
            })}
          </Grid>
        }
      />
    </Card>
  );
}

SubjectCard.defaultProps = {
  subject: {
    name: "",
    daysOfWeek: "",
    trainer: "",
    schedule: "",
    startDate: "",
    image: "",
  },
};

SubjectCard.propTypes = {
  subject: PropTypes.shape({
    name: PropTypes.string,
    daysOfWeek: PropTypes.string,
    trainer: PropTypes.string,
    schedule: PropTypes.string,
    startDate: PropTypes.string,
    image: PropTypes.string,
  }),
};

export default SubjectCard;
