import {
  Button,
  Card,
  CardContent,
  Checkbox,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  RadioGroup,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import PropTypes from "prop-types";
import React from "react";
import validateInfo from "../../helper/validateInfo";
import useForm from "../../hooks/useForm";
import SeparatorBar from "../SeparatorBar";

function Form({ inputs, onSubmit }) {
  const { title, sections, radios } = inputs;
  const initialValues = {};
  for (let i = 0; i < sections.length; i += 1) {
    sections[i].inputs.map(({ label, value }) => {
      initialValues[label] = value;
      return null;
    });
  }
  for (let i = 0; i < radios.length; i += 1) {
    initialValues[radios[i].title] = 0;
  }

  const validations = [];
  for (let i = 0; i < sections.length; i += 1) {
    sections[i].inputs.map(({ label, validation }) => {
      validations.push({ label, validation });
      return null;
    });
    radios.map(({ validation, ...rest }) => {
      validations.push({ label: rest.title, validation });
      return null;
    });
  }

  const { handleChange, handleRadioChange, handleSubmit, values, errors } =
    useForm(initialValues, onSubmit, validateInfo, validations);

  const renderForm = () => {
    return (
      <>
        {sections.map((s) => {
          return (
            <div key={s.id}>
              <Typography variant="subtitle1">{s.title}</Typography>
              <Grid container spacing={1}>
                {s.inputs.map((i) => {
                  return (
                    <Grid item key={i.id} xs={i.xs} sm={i.sm} md={i.md}>
                      <InputLabel
                        required={i.required}
                        error={errors[i.label] !== undefined}
                      >
                        {i.label}
                      </InputLabel>
                      {i.type === "file" && (
                        <TextField
                          type={i.type}
                          placeholder={i.placeholder}
                          onChange={handleChange}
                          name={i.label}
                          error={errors[i.label] !== undefined}
                          fullWidth
                        />
                      )}
                      {i.type === "select" && (
                        <Select
                          id={i.label}
                          value={values[i.label]}
                          label={i.label}
                          onChange={handleChange}
                          name={i.label}
                          error={errors[i.label] !== undefined}
                          fullWidth
                        >
                          {i.selectItems.map((item) => {
                            return (
                              <MenuItem key={item.value} value={item.value}>
                                {item.label}
                              </MenuItem>
                            );
                          })}
                        </Select>
                      )}
                      {i.type !== "select" && i.type !== "file" && (
                        <TextField
                          type={i.type}
                          placeholder={i.placeholder}
                          onChange={handleChange}
                          value={values[i.label]}
                          name={i.label}
                          error={errors[i.label] !== undefined}
                          fullWidth
                        />
                      )}
                      {errors[i.label] && (
                        <FormHelperText error>{errors[i.label]}</FormHelperText>
                      )}
                    </Grid>
                  );
                })}
              </Grid>
            </div>
          );
        })}
        {radios.map((r) => {
          return (
            <div key={r.id}>
              <Typography variant="subtitle1">{r.title}:</Typography>
              <RadioGroup row>
                {r.values.map((v) => {
                  return (
                    <FormControlLabel
                      key={v.id}
                      value={v.value}
                      control={<Checkbox />}
                      label={v.label}
                      onChange={handleRadioChange}
                      name={r.title}
                    />
                  );
                })}
              </RadioGroup>
              {errors[r.title] && (
                <FormHelperText error>{errors[r.title]}</FormHelperText>
              )}
            </div>
          );
        })}
        <Button
          type="submit"
          variant="contained"
          color="primary"
          style={{ width: "160px", marginTop: "1em" }}
          onClick={() => {}}
        >
          Submit
        </Button>
      </>
    );
  };

  return (
    <>
      <SeparatorBar title={title} />
      <Card style={{ margin: "0 auto" }}>
        <CardContent>
          <form onSubmit={handleSubmit} noValidate>
            <FormGroup>{renderForm()}</FormGroup>
          </form>
        </CardContent>
      </Card>
    </>
  );
}

Form.propTypes = {
  inputs: PropTypes.shape({
    title: PropTypes.string,
    sections: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        inputs: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            placeholder: PropTypes.string,
            label: PropTypes.string,
            required: PropTypes.bool,
            type: PropTypes.string,
            selectItems: PropTypes.arrayOf(
              PropTypes.shape({
                value: PropTypes.number,
                label: PropTypes.string,
              }),
            ),
            xs: PropTypes.number,
            sm: PropTypes.number,
            md: PropTypes.number,
            validation: PropTypes.shape({
              isRequired: PropTypes.bool,
              test: PropTypes.func,
              message: PropTypes.string,
            }),
          }),
        ),
      }),
    ),
    radios: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        values: PropTypes.arrayOf(
          PropTypes.shape({
            id: PropTypes.number,
            value: PropTypes.number,
            label: PropTypes.string,
          }),
        ),
        validation: PropTypes.shape({
          isRequired: PropTypes.bool,
          test: PropTypes.func,
          message: PropTypes.string,
        }),
      }),
    ),
  }).isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default Form;
