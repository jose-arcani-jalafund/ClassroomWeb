import React from "react";
import { Box, Container, Stack, Typography } from "@mui/material";
import { useStudentPageContext } from "../../contexts/studentPageContext";
import getDaySchedule from "../../helper/getDaySchedule";
import SubjectSmallCard from "../SubjectSmallCard";
import DayCard from "../DayCard";

function WeeklySchedule() {
  const { course } = useStudentPageContext();

  const subjects = getDaySchedule(course && course.subjects);

  const renderSchedule = () => {
    if (!subjects) return <div />;
    const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
    return (
      <Stack direction="row" spacing={2}>
        {days.map((day) => {
          return (
            <Stack key={day} spacing={2}>
              <DayCard title={day} />
              {subjects.map((s) => {
                return (
                  s.daysOfWeek.includes(day.slice(0, 2)) && (
                    <SubjectSmallCard key={s.id} subject={s} />
                  )
                );
              })}
            </Stack>
          );
        })}
      </Stack>
    );
  };

  return !course ? (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      minHeight="100px"
    >
      <Typography variant="h5">
        Student is not registered to a course!
      </Typography>
    </Box>
  ) : (
    <Container sx={{ my: 3, background: "#9bddff", p: 3 }}>
      {renderSchedule()}
    </Container>
  );
}

export default WeeklySchedule;
