import { useMemo, useState } from "react";

const useFilterList = (initialValues, itemList) => {
  const [values, setValues] = useState(initialValues);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleRadioChange = (event) => {
    const { name, value, checked } = event.target;
    if (checked) {
      setValues({
        ...values,
        [name]: values[name] + Number(value),
      });
    } else {
      setValues({
        ...values,
        [name]: values[name] - Number(value),
      });
    }
  };
  const filteredList = useMemo(() => {
    let listCopy = [...itemList];
    if (values["Filter Name"] !== "") {
      listCopy = listCopy.filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(values["Filter Name"].toLowerCase());
      });
    }

    if (values.Country !== 0) {
      listCopy = listCopy.filter((item) => {
        return item.countryId === values.Country;
      });
    }

    // filter by status
    if (typeof values["Filter by status"] === "number") {
      switch (values["Filter by status"]) {
        case 0: {
          break;
        }
        case 1: {
          listCopy = listCopy.filter((item) => {
            return item.status === "Active";
          });
          break;
        }
        case 2: {
          listCopy = listCopy.filter((item) => {
            return item.status === "Inactive";
          });
          break;
        }
        case 3: {
          listCopy = listCopy.filter((item) => {
            return item.status === "Inactive" || item.status === "Active";
          });
          break;
        }
        case 4: {
          listCopy = listCopy.filter((item) => {
            return item.status === "Finished";
          });
          break;
        }
        case 5: {
          listCopy = listCopy.filter((item) => {
            return item.status === "Finished" || item.status === "Active";
          });
          break;
        }
        case 6: {
          listCopy = listCopy.filter((item) => {
            return item.status === "Finished" || item.status === "Inactive";
          });
          break;
        }
        default: {
          break;
        }
      }
    }

    // filter by date
    // if (
    //   typeof values["From Date"] === "string" ||
    //   values["From Date"].lenght === 0
    // ) {
    //   let filteredData = coursesCopy;
    //   filteredData = coursesCopy.filter((course) => {
    //     const date = new Date(course.startingDate).toISOString().slice(0, 10);
    //     return date >= values["From Date"];
    //   });

    //   return filteredData;
    // }
    return listCopy;
  }, [itemList, values]);

  return { handleChange, handleRadioChange, values, filteredList };
};

export default useFilterList;
