import getDateString from "../helper/getDateString";
import getDaysString from "../helper/getDaysString";

const createSubjectListAdapter = (data) => {
  const subjectList = [];

  if (data !== undefined) {
    for (let i = 0; i < data.length; i += 1) {
      subjectList.push({
        id: data[i].id,
        name: data[i].subjectName,
        daysOfWeek: getDaysString(data[i].daysOfWeek),
        trainer: data[i].trainerName,
        schedule: `${data[i].startTime} to ${data[i].endTime}`,
        startDate: getDateString(data[i].startingDate),
        image: data[i].image,
      });
    }
  }

  return subjectList;
};

export default createSubjectListAdapter;
