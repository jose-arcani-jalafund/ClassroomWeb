const getDaySchedule = (subjects) => {
  if (subjects) {
    const orderedSubjects = subjects.sort((a, b) => {
      if (a.schedule.slice(0, 4) < b.schedule.slice(0, 4)) return -1;
      if (b.schedule.slice(0, 4) < a.schedule.slice(0, 4)) return 1;
      return 0;
    });
    return orderedSubjects;
  }
  return null;
};

export default getDaySchedule;
